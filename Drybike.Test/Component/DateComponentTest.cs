using System;
using System.Collections.Generic;
using DryBike.Domain;
using DryBike.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Drybike.Test
{
    [TestClass]
    public class DateComponentTest
    {
        private IDateComponent subject = new DateComponent();

        static object[] dayDayaTest
        {
            get => new[]
            {
                new object[] {new List<DayOfWeek>{DayOfWeek.Monday,DayOfWeek.Saturday,DayOfWeek.Thursday}, "Mon Thu Sat"},
                new object[] {new List<DayOfWeek>{DayOfWeek.Monday,DayOfWeek.Tuesday,DayOfWeek.Wednesday}, "Mon Tue Wed"},
                new object[] {new List<DayOfWeek>{DayOfWeek.Monday,DayOfWeek.Tuesday,DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday}, "Sun Mon Tue Wed Thu Fri Sat"}
            };
        }

        [TestMethod]
        [DynamicData("dayDayaTest")]
        public void testdayListToLetters(List<DayOfWeek> days, string expected)
        {
            Assert.AreEqual(expected , subject.dayListToLetters(days));
        }

        [TestMethod]
        public void testisDisplayableDate()
        {
            DateTimeOffset now = DateTimeOffset.Now;
            DateTimeOffset past = DateTimeOffset.Now.AddMonths(-1);
            DateTimeOffset okDate = DateTimeOffset.Now.AddDays(2);
            DateTimeOffset noOkDate = DateTimeOffset.Now.AddMonths(1);

            Assert.IsTrue(subject.isDisplayableDate(okDate));
            Assert.IsFalse(subject.isDisplayableDate(now));
            Assert.IsFalse(subject.isDisplayableDate(past));
            Assert.IsFalse(subject.isDisplayableDate(noOkDate));
        }

        [TestMethod]
        public void testgetAllPosibleTime()
        {
            UserPath up = new UserPath("test");
            up.startPathTime = new DateTimeOffset().AddHours(5).AddMinutes(30);
            up.weekDayActive = new List<DayOfWeek> { DayOfWeek.Monday, DayOfWeek.Saturday};

            DateTimeOffset now = DateTimeOffset.Now;

            List<DateTimeOffset> result = subject.getAllPosibleTime(now, up);

            Assert.AreEqual(2,result.Count);
            Assert.AreEqual(DayOfWeek.Monday, result[0].DayOfWeek);
            Assert.AreEqual(DayOfWeek.Saturday, result[1].DayOfWeek);

        }
    }
}
