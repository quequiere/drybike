﻿using DryBike.Models.OpenWeatherModels;
using DryBike.Web.weather;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Drybike.Test.Web.Weather
{

    [TestClass]
    public class OpenWeatherServiceTest
    {
        private IWeatherService subject = new OpenWeatherService("63a7cbadae092bf6aa205f2169cdf9be");

        [TestMethod]
        [Ignore("This is not a real test, just to direct call api for debug")]
        public async Task TestRealCallAsync()
        {
            WeatherEntity result =  await subject.GetWeatherAsync(new GeoCoordinate(48.862358, 2.351950));
            Console.WriteLine(result);
        }
    }
}
