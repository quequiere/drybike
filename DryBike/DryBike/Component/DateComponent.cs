﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using Android.Widget;
using DryBike.Domain;

namespace DryBike.Services
{
    public interface IDateComponent
    {
        string dayListToLetters(List<DayOfWeek> days);
        DayOfWeek getDayFromString(string day);

        bool isDisplayableDate(DateTimeOffset targetTime);

        List<DateTimeOffset> getAllPosibleTime(DateTimeOffset now, UserPath path);

        List<Tuple<DateTimeOffset, UserPath>> getDisplayablePath(List<UserPath> userPaths);
    }
    public class DateComponent : IDateComponent
    {
        public string dayListToLetters(List<DayOfWeek> days)
        {
            CultureInfo en = new CultureInfo("en-US");
            string[] abbreviatedDay = en.DateTimeFormat.AbbreviatedDayNames;
            return string.Join(" ", days.OrderBy(d => d).Select(d => abbreviatedDay[(int) d]));
        }

        public DayOfWeek getDayFromString(string day)
        {
            switch(day)
            {
                case "Sun": return DayOfWeek.Sunday;
                case "Mon": return DayOfWeek.Monday;
                case "Tue": return DayOfWeek.Tuesday;
                case "Wed": return DayOfWeek.Wednesday;
                case "Thu": return DayOfWeek.Thursday;
                case "Fri": return DayOfWeek.Friday;
                case "Sat": return DayOfWeek.Saturday;
            };

            throw new Exception("Error, day doesn't exist: " + day);
        }
        
        //we check if the target date is less than 5 days
        public bool isDisplayableDate(DateTimeOffset targetTime)
        {
            TimeSpan difference = targetTime - DateTimeOffset.Now;

            if (difference.Days > 5 || difference.TotalMilliseconds<0)
                return false;
            return true;
        }

        public List<DateTimeOffset> getAllPosibleTime(DateTimeOffset now, UserPath path)
        {
            List<DateTimeOffset> possibleDates = new List<DateTimeOffset>();

            DayOfWeek cuurentDay = now.DayOfWeek;

            foreach (DayOfWeek targetDay in path.weekDayActive)
            {
                int diff = targetDay - cuurentDay;
                if(diff < 0)
                {
                    diff += 7;
                }

                try
                {

                    

                    int day = (now.Day + diff);
                    int maxDay = DateTime.DaysInMonth(now.Year, now.Month);
                    int mounth = now.Month;
                    int year = now.Year;

                    if (day > maxDay)
                    {
                        day = day - DateTime.DaysInMonth(now.Year, now.Month);
                        mounth++;
                        if (mounth>12)
                        {
                            year++;
                            mounth = 1;
                        }
                    }


                    DateTimeOffset date = new DateTimeOffset(year, mounth, day , path.startPathTime.Hour, path.startPathTime.Minute, 0, new TimeSpan());
                    possibleDates.Add(date);
                }
                catch(Exception e)
                {
                    Debug.WriteLine("Error while creating date " + e.Message);
                    throw e;
                }
              
            }

            return possibleDates;
        }

        public List<Tuple<DateTimeOffset, UserPath>> getDisplayablePath(List<UserPath> userPaths)
        {
            List<Tuple<DateTimeOffset, UserPath>> list = new List<Tuple<DateTimeOffset, UserPath>>();

            foreach (UserPath path in userPaths.Where(p => p.isActive && p.RouteCoordinates.Count>0).ToList())
            {
                getAllPosibleTime(DateTimeOffset.Now, path).ForEach(time => list.Add(Tuple.Create(time, path)));
            }
            return list;
        }

    }
}
