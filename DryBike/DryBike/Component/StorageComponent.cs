﻿using DryBike.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DryBike.Component
{
    public interface IStorageComponent
    {
        void saveUserPath(List<UserPath> paths);

        List<UserPath> getSavedUserPath();
    }
    public class StorageComponent : IStorageComponent
    {
        public List<UserPath> getSavedUserPath()
        {
            try
            {
                string text = File.ReadAllText(getFileNameAndPath());
                return JsonConvert.DeserializeObject<List<UserPath>>(text);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error while reading json "+e.Message);
                return new List<UserPath>();
            }
            
        }

        public void saveUserPath(List<UserPath> paths)
        {
            try
            {
                Debug.WriteLine("Save userpaths to storage in file: " + this.getFileNameAndPath());
                File.WriteAllText(this.getFileNameAndPath(), JsonConvert.SerializeObject(paths));
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error while save user path list " + e.Message);
            }
         
        }

        private string getFileNameAndPath()
        {
            try
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "userpath.json");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error while find path " + e.Message);
                throw e;
            }
   
          
        }
    }
}
