﻿using DryBike.Domain;
using DryBike.Models.OpenWeatherModels;
using DryBike.Web.weather;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using System.Linq;
using DryBike.Models;

namespace DryBike.Component
{
    public interface IWeatherComponent
    {
        Task<WeatherEntity> refreshWeatherPathAsync(GeoCoordinate geo);

        Task<WeatherForecastEntity> getWorstWeatherConditionForPath(List<Position> coordinates,
            DateTimeOffset neededTime);

        Task<WeatherResume> refreshWeatherResumeAsync(WeatherResume resume);
    }


    public class WeatherComponent : IWeatherComponent
    {
        private IWeatherService weatherApi;
        private IPathComponent pathComponent;

        public WeatherComponent(IPathComponent pathComponent)
        {
            this.pathComponent = pathComponent;
        }


        public async Task<WeatherEntity> refreshWeatherPathAsync(GeoCoordinate geo)
        {
            weatherApi = new OpenWeatherService("63a7cbadae092bf6aa205f2169cdf9be");
            WeatherEntity lastWeather = await weatherApi.GetWeatherAsync(geo);
            return lastWeather;
        }

        public async Task<WeatherForecastEntity> getWorstWeatherConditionForPath(List<Position> coordinates,
            DateTimeOffset neededTime)
        {
            List<WeatherForecastEntity> weatherResults = new List<WeatherForecastEntity>();


            foreach (Position pos in coordinates)
            {
                WeatherEntity result = await refreshWeatherPathAsync(new GeoCoordinate(pos.Latitude, pos.Longitude));

                if (result == null)
                {
                    throw new Exception("Error while getting weather");
                }

                weatherResults.Add(getBetterHourWeather(result, neededTime));
            }

            //Now we want the wrost condition
            return weatherResults.OrderByDescending(c => c.Condition).First();
        }

        public WeatherForecastEntity getBetterHourWeather(WeatherEntity weather, DateTimeOffset targetTime)
        {
            WeatherForecastEntity better = null;
            long minorDiff = long.MaxValue;

            foreach (WeatherForecastEntity current in weather.Forecasts)
            {
                long diff = Math.Abs(
                    current.ForecastDate.ToUnixTimeMilliseconds() - targetTime.ToUnixTimeMilliseconds());
                if (better == null || diff < minorDiff)
                {
                    better = current;
                    minorDiff = diff;
                }
            }

            return better;
        }


        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public async Task<WeatherResume> refreshWeatherResumeAsync(WeatherResume resume)
        {
            Debug.WriteLine("Start check weather");

            await Task.Delay(1000 * RandomNumber(2,5));

            WeatherForecastEntity result =
                await getWorstWeatherConditionForPath(resume.originalPath.RouteCoordinates, resume.startDate);


            Debug.WriteLine("Find condition " + result.Condition + " for path " + resume.originalPath.name);

            resume.imageType = getWeatherImg(result.Condition);

            return resume;
        }


        private String getWeatherImg(WeatherCondition condition)
        {
            int weatherCondition = (int) condition;

            if (weatherCondition == 0)
                return "notload.png";
            else if (weatherCondition <= 3)
                return "sun.png";
            else if(weatherCondition <=6)
                return "cloud.png";
            else if (weatherCondition <= 7)
                return "fog.png";
            else if (weatherCondition <= 9)
                return "rain.png";
            else if (weatherCondition <= 11)
                return "snow.png";
            else if (weatherCondition <= 13)
                return "thunder.png";

            throw new Exception("Unknow img for condiction "+condition);
        }
    }
}