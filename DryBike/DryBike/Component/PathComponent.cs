﻿using DryBike.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DryBike.Models;
using DryBike.Services;

namespace DryBike.Component
{
    public interface IPathComponent
    {
        List<UserPath> GetUserPaths();

        UserPath getPathByName(String pathName);

        void setEnablePath(UserPath path, bool enable);

        bool renamePath(UserPath path, string name);

        string getPathDayDescription(UserPath path);

        bool addPath(UserPath path);

        UserPathModel generateModel(String name);

        void savePath();
    }

    public class PathComponent : IPathComponent
    {
        private List<UserPath> registeredPath;
        private IDateComponent dateComponent;
        private IStorageComponent storageComponent;

        public PathComponent(IDateComponent dateComponent, IStorageComponent storage)
        {
            this.dateComponent = dateComponent;
            this.storageComponent = storage;
        }

        public List<UserPath> GetUserPaths()
        {
            if (registeredPath == null)
            {
                registeredPath = storageComponent.getSavedUserPath();
            }
            return registeredPath;
        }

        public UserPath getPathByName(String pathName)
        {
            return GetUserPaths().FirstOrDefault(p => p.name.Equals(pathName));
        }

        public UserPathModel generateModel(String name)
        {
            UserPath u = this.getPathByName(name);
            if (u == null)
                return null;

            string convertedPathStart = u.startPathTime.ToString("HH:mm");

            return new UserPathModel(u.name,this.dateComponent.dayListToLetters(u.weekDayActive),u.RouteCoordinates.Count, convertedPathStart,u.isActive);
        }

        public void setEnablePath(UserPath path, bool enable)
        {
            path.isActive = enable;
        }

        public bool renamePath(UserPath path, string name)
        {
            if (getPathByName(name) != null)
                return false;

            path.name = name;
            return true;
        }

        public string getPathDayDescription(UserPath path)
        {
            return this.dateComponent.dayListToLetters(path.weekDayActive);
        }

        public bool addPath(UserPath path)
        {
            if (this.getPathByName(path.name) != null || path.name ==null || path.name.Length<=0)
                return false;

            this.GetUserPaths().Add(path);
            this.savePath();
            return true;
        }

        public void savePath()
        {
            this.storageComponent.saveUserPath(this.GetUserPaths());
        }
    }
}
