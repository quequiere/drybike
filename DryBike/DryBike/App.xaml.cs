﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DryBike.Services;
using DryBike.Views;
using Autofac;
using DryBike.Views.General;
using System.Threading.Tasks;
using System.Diagnostics;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DryBike
{
    public partial class App : Application
    {
        public static App instance;

        public IContainer container;

        public App()
        {
            InitializeComponent();

            instance = this;

            var builder = new ContainerBuilder();

            Startup startup = new Startup();
            startup.configureService(builder);
            container = builder.Build();


            MainPage = new SplashPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {

        }

        protected override void OnResume()
        {
        }
    }
}
