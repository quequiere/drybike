﻿using System;
using System.Collections.Generic;
using System.Text;
using DryBike.Domain;
using Xamarin.Forms.Maps;

namespace DryBike.Services
{

    public class CustomMapRender : Map
    {
        public event EventHandler<MapTapEventArgs> Tapped;
        public event EventHandler<MapUpdatePolygoneEvent> UpdatePolygone;

    
        public List<Position> RouteCoordinates { get; set; }

        public CustomMapRender()
        {
            RouteCoordinates = new List<Position>();
        }


        public void updatedRoute(List<Position> position)
        {
            RouteCoordinates = position;
            this.OnUpdatePolygoneEvent(new MapUpdatePolygoneEvent());
        }

        public void OnTap(Position coordinate)
        {
            OnTapEvent(new MapTapEventArgs {Position = coordinate});
        }

        protected virtual void OnTapEvent(MapTapEventArgs e)
        {
            var handler = Tapped;

            if (handler != null)
                handler(this, e);
        }

        public virtual void OnUpdatePolygoneEvent(MapUpdatePolygoneEvent e)
        {
            var handler = UpdatePolygone;

            if (handler != null)
                handler(this, e);
        }
    }

    public class MapTapEventArgs : EventArgs
    {
        public Position Position { get; set; }
    }

    public class MapUpdatePolygoneEvent : EventArgs
    {

    }
}
