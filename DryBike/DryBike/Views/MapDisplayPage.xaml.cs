﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autofac;
using DryBike.Component;
using DryBike.Domain;
using DryBike.Services;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapDisplayPage : ContentPage
    {
        public UserPath userPath;
        private IPathComponent pathComponent;

        public MapDisplayPage(UserPath userPath)
        {
            this.userPath = userPath;

            App app = (App)Application.Current;
            this.pathComponent = app.container.Resolve<IPathComponent>();

            Title = this.userPath.name;

            InitializeComponent();
            //TODO center map here
            map.Tapped += Map_Tapped;
            
            //copy list to prevent modify for cancel option
            map.updatedRoute(new List<Position>(userPath.RouteCoordinates));

            Debug.WriteLine("Map loaded with locnumber: "+ userPath.RouteCoordinates.Count);

            BindingContext = this;
        }

        private void Map_Tapped(object sender, Services.MapTapEventArgs e)
        {
            map.RouteCoordinates.Add(e.Position);

            map.OnUpdatePolygoneEvent(new MapUpdatePolygoneEvent());
        }

        async void Button_SaveClicked(object sender, EventArgs e)
        {
            userPath.RouteCoordinates = map.RouteCoordinates;
            Debug.WriteLine("Saved user path "+ userPath.RouteCoordinates.Count);
            MessagingCenter.Send(this, "refreshDetailsFromMap", userPath);
            await Navigation.PopModalAsync();

            pathComponent.savePath();

        }

        private void Button_ClearPathClicked(object sender, EventArgs e)
        {
            map.Pins.Clear();
            map.RouteCoordinates.Clear();
            map.OnUpdatePolygoneEvent(new MapUpdatePolygoneEvent());
        }

        async void Button_CancelClicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}