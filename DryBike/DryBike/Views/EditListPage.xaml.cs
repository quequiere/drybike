﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Autofac;
using DryBike.Component;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DryBike.Domain;
using DryBike.Models;
using System.Linq;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditListPage : ContentPage
    {
        private readonly IPathComponent pathComponent;
        public ObservableCollection<UserPathModel> userPathModel { get; set; }

        public Command LoadUserPathCommand { get; set; }



        public EditListPage()
        {
            InitializeComponent();

            App app = (App)Application.Current;
            this.pathComponent = app.container.Resolve<IPathComponent>();

            userPathModel = new ObservableCollection<UserPathModel>();
            BindingContext = this;

            Title = "Path Editor";


            LoadUserPathCommand = new Command(async () => await ExecuteLoadUserPathCommand());

            MessagingCenter.Subscribe<NewUserPathPage, UserPath>(this, "refreshPathList", async (obj, path) =>
            {
                LoadUserPathCommand.Execute(null);
            });

            MessagingCenter.Subscribe<PathDetailPage>(this, "refreshPathList", async (obj) =>
            {
                LoadUserPathCommand.Execute(null);
            });


            if (this.pathComponent.GetUserPaths().Count == 0)
            {
                Application.Current.MainPage.DisplayAlert("Tips", "You have no path defined yet. Use '+' on the screen top to add a new path.", "Got it !");
            }


        }


        async Task ExecuteLoadUserPathCommand()
        {
            userPathModel.Clear();
            pathComponent.GetUserPaths().ForEach(p =>
            {
                UserPathModel model = this.pathComponent.generateModel(p.name);
                userPathModel.Add(model);
            });
        }


        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            UserPathModel pathModel = args.SelectedItem as UserPathModel;
            if (pathModel == null)
            {
                return;
            }

            await Navigation.PushAsync(new PathDetailPage(pathModel.pathName));

            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewUserPathPage(new UserPath(""))));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (userPathModel.Count == 0)
                LoadUserPathCommand.Execute(null);
        }

        private void Switch_Toggled(object sender, ToggledEventArgs e)
        {
            foreach(UserPathModel model in userPathModel)
            {
                UserPath path = this.pathComponent.getPathByName(model.pathName);
                path.isActive = model.isActive;
            }

            this.pathComponent.savePath();
        }
    }
}