﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Autofac;
using DryBike.Models;
using DryBike.Component;
using DryBike.Domain;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PathDetailPage : ContentPage, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public UserPathModel path { get; set; }
        private IPathComponent pathComponent;

        public PathDetailPage(String pathName)
        {
            InitializeComponent();

            App app = (App)Application.Current;
            this.pathComponent = app.container.Resolve<IPathComponent>();

            this.Title = "Path detail";

            this.path = pathComponent.generateModel(pathName);

            Debug.WriteLine("Loaded with path length "+this.path.pathLocationNumber);
            Debug.WriteLine("Loaded start" + this.path.pathStart);


            if (this.path.pathLocationNumber == 0)
                AlertMap.IsVisible = true;

            BindingContext = this;


            MessagingCenter.Subscribe<NewUserPathPage, UserPath>(this, "refreshDetails", async (obj, updatedPath) =>
            {
                Debug.WriteLine("Refesh detail asked by map new user page");
                this.path = this.pathComponent.generateModel(updatedPath.name);
                this.OnPropertyChanged("path");
            });

            MessagingCenter.Subscribe<MapDisplayPage, UserPath>(this, "refreshDetailsFromMap", async (obj, updatedPath) =>
            {
                Debug.WriteLine("Refesh detail asked by map Page "+updatedPath.RouteCoordinates.Count);
                this.path = this.pathComponent.generateModel(updatedPath.name);

                if (this.path.pathLocationNumber == 0)
                    AlertMap.IsVisible = true;
                else
                    AlertMap.IsVisible = false;

                this.OnPropertyChanged("path");
            });

        }

        private async void OnEditProfileClicked(object sender, EventArgs e)
        {

            UserPath upath = pathComponent.getPathByName(path.pathName);

            if(upath == null)
            {
                await DisplayAlert("Alert", "Error, this path doesn't exist anymore", "OK");
                return;
            }
                
            await Navigation.PushModalAsync(new NavigationPage(new NewUserPathPage(upath,true)));
        }

        private void OnEditPathMapClicked(object sender, EventArgs e)
        {
            this.openMapEditor();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        async void openMapEditor()
        {
            UserPath upath = pathComponent.getPathByName(path.pathName);

            if (upath == null)
            {
                await DisplayAlert("Alert", "Error, this path doesn't exist anymore", "OK");
                return;
            }
            await Navigation.PushModalAsync(new NavigationPage(new MapDisplayPage(upath)));

        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            UserPath p = this.pathComponent.getPathByName(this.path.pathName);

            this.pathComponent.GetUserPaths().Remove(p);

            this.pathComponent.savePath();

            MessagingCenter.Send(this, "refreshPathList");

            await Navigation.PopAsync();
        }
    }
}