﻿using DryBike.Domain;
using DryBike.Views.General;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.PathEditor:
                        MenuPages.Add(id, new NavigationPage(new EditListPage()));
                        break;
                    case (int)MenuItemType.MyWeek:
                        MenuPages.Add(id, new NavigationPage(new PathListDisplayPage()));
                        break;
                    case (int)MenuItemType.About:
                        MenuPages.Add(id, new NavigationPage(new AboutDBPage()));
                        break;
                }
            }

            NavigationPage newPage = MenuPages[id];

            if ((int) MenuItemType.MyWeek == id)
            {
                newPage = new NavigationPage(new PathListDisplayPage());
            }

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}