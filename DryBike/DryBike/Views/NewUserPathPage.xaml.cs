﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autofac;
using DryBike.Component;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DryBike.Domain;
using DryBike.Services;
using IntelliAbb.Xamarin.Controls;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewUserPathPage : ContentPage
    {
        public UserPath UserPath { get; set; }

        private IDateComponent _dateComponent;
        private IPathComponent pathComponent;
        
        private bool editMode = false;

        public NewUserPathPage(UserPath userPath, bool editMode = false)
        {
            App app = (App)Application.Current;
            this._dateComponent = app.container.Resolve<IDateComponent>();
            this.pathComponent = app.container.Resolve<IPathComponent>();

            this.editMode = editMode;

            InitializeComponent();

            UserPath = userPath;

            TimePickerStart.Time = new TimeSpan(UserPath.startPathTime.Hour, UserPath.startPathTime.Minute, 00);



            Grid grid = this.FindByName<Grid>("dayList");

            foreach (Checkbox box in grid.Children.Where(c => c is Checkbox))
            {
                DayOfWeek targetDay = _dateComponent.getDayFromString(box.StyleId);

                if (UserPath.weekDayActive.Contains(targetDay))
                    box.IsChecked = true;
                else
                    box.IsChecked = false;
            }

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
           

            Grid grid = this.FindByName<Grid>("dayList");

            UserPath.weekDayActive.Clear();

            foreach (Checkbox box in grid.Children.Where(c => c is Checkbox))
            {
                DayOfWeek targetDay = _dateComponent.getDayFromString(box.StyleId);

                if (box.IsChecked)
                {
                    UserPath.weekDayActive.Add(targetDay);
                }
            }
          
            UserPath.startPathTime= new DateTimeOffset().AddHours(TimePickerStart.Time.Hours).AddMinutes(TimePickerStart.Time.Minutes);
            
            if (!editMode)
            {
                if (pathComponent.addPath(UserPath))
                {
                    MessagingCenter.Send(this, "refreshPathList", UserPath);
                    await Navigation.PopModalAsync();
                }
                else
                {
                    await DisplayAlert("Alert", "This path name already exist", "OK");
                }
            }
            else
            {
                MessagingCenter.Send(this, "refreshDetails", UserPath);
                MessagingCenter.Send(this, "refreshPathList", UserPath);
                await Navigation.PopModalAsync();
            }

            pathComponent.savePath();
        }


        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}