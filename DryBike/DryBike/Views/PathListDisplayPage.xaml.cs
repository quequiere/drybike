﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Autofac;
using DryBike.Component;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DryBike.Domain;
using DryBike.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Data;
using DryBike.Models.OpenWeatherModels;
using DryBike.Services;
using Plugin.Connectivity;
using Xamarin.Forms.Internals;

namespace DryBike.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PathListDisplayPage : ContentPage
    {
        private readonly IPathComponent pathComponent;
        private readonly IDateComponent dateComponent;
        private readonly IWeatherComponent weatherComponent;



        public ObservableCollection<WeatherResume> weatherResumes { get; set; }



        public PathListDisplayPage()
        {

            Debug.WriteLine("Created path list display");


            InitializeComponent();

            App app = (App) Application.Current;
            this.pathComponent = app.container.Resolve<IPathComponent>();
            this.dateComponent = app.container.Resolve<IDateComponent>();
            this.weatherComponent = app.container.Resolve<IWeatherComponent>();

            Title = "Your week";


            weatherResumes = new ObservableCollection<WeatherResume>();

            BindingContext = this;

            this.refreshPage();

        }

        protected override void OnAppearing()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                Application.Current.MainPage.DisplayAlert("Alert", "Please check you internet connection", "OK");
            }

            base.OnAppearing();

        }



        public void refreshPage()
        {
            

            List<Tuple<DateTimeOffset, UserPath>> list = dateComponent.getDisplayablePath(pathComponent.GetUserPaths());


            if (list.Count == 0)
            {
                Label l = new Label();
                Label l2 = new Label();
      

                l.HorizontalTextAlignment = TextAlignment.Center;
                l2.HorizontalTextAlignment = TextAlignment.Center;


                l.FontSize = 32;
                l2.FontSize = 16;

                l.Text = "No path defined or actived";
                l2.Text = "You can add them from path editor in context menu";




                StackLayout sl = new StackLayout();
                sl.HorizontalOptions = LayoutOptions.CenterAndExpand;
                sl.VerticalOptions = LayoutOptions.CenterAndExpand;
                sl.Children.Add(l);
                sl.Children.Add(l2);
 
                Content = sl;

                weatherResumes.Clear();

                return;
            }

       

            Content = dataList;

            List<WeatherResume> tempList = new List<WeatherResume>();

            //Create a line for all path
            foreach (Tuple<DateTimeOffset, UserPath> t in list)
            {
                tempList.Add(new WeatherResume()
                {
                    originalPath = t.Item2,
                    startDate = t.Item1,
                    imageType = "notload.png",
                    dayOfTheWeek = DateTimeFormatInfo.CurrentInfo.GetDayName(t.Item1.DayOfWeek)  + " "+t.Item1.ToString("MMMM, dd HH:mm")

                });
            }

            weatherResumes.Clear();

            //Order them by date
            tempList.OrderBy(r => r.startDate.ToUnixTimeSeconds()).ToList().ForEach(r => weatherResumes.Add(r));

           this.refreshList(tempList);

        }

        //This method is totally ugly, i know. But the original one doesn't support android platform. So back to sync call to display weather
        private async Task refreshList(List<WeatherResume> tempList)
        {
            ItemsListView.BeginRefresh();

            if (!CrossConnectivity.Current.IsConnected)
            {
                Debug.WriteLine("Can't connect on web");
                return;
            }


            List<Task<WeatherResume>> currentTask = new List<Task<WeatherResume>>();

            Parallel.ForEach(tempList,  w =>
            {
                currentTask.Add(weatherComponent.refreshWeatherResumeAsync(w));
            });

            await Task.WhenAll(currentTask);

            weatherResumes.Clear();
    
            List< WeatherResume> ordered =  currentTask.OrderBy(c => c.Result.startDate.ToUnixTimeSeconds()).Select(c => c.Result).ToList();


            foreach (var r in ordered)
            {
                
                await Task.Delay(100);
                weatherResumes.Add(r);

                //Very ugly but necessary for android or list doesn't update...I spend 5 hours on this bug
                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);
            }

            ItemsListView.EndRefresh();
        }

    }
}