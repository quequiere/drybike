﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using DryBike.Component;
using DryBike.Services;

namespace DryBike
{
    public class Startup
    {
        public void configureService(ContainerBuilder builder)
        {
            builder.RegisterType<PathComponent>().As<IPathComponent>().SingleInstance();
            builder.RegisterType<WeatherComponent>().As<IWeatherComponent>().SingleInstance();
            builder.RegisterType<DateComponent>().As<IDateComponent>();
            builder.RegisterType<StorageComponent>().As<IStorageComponent>();
        }
    }
}
