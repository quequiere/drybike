﻿using DryBike.Models.OpenWeatherModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace DryBike.Domain
{
    public class UserPath
    {

        public String name { get; set; }

        public List<DayOfWeek> weekDayActive { get; set; }

        public bool isActive { get; set; }

        public List<Position> RouteCoordinates { get; set; } = new List<Position>();

        public DateTimeOffset startPathTime ;

        public UserPath(string name)
        {
            this.name = name;
            this.isActive = true;

            this.weekDayActive = new List<DayOfWeek>();

            this.weekDayActive.Add(DayOfWeek.Sunday);
            this.weekDayActive.Add(DayOfWeek.Monday);
            this.weekDayActive.Add(DayOfWeek.Tuesday);
            this.weekDayActive.Add(DayOfWeek.Wednesday);
            this.weekDayActive.Add(DayOfWeek.Thursday);
            this.weekDayActive.Add(DayOfWeek.Friday);
            this.weekDayActive.Add(DayOfWeek.Saturday);

            this.startPathTime = new DateTimeOffset().AddHours(8).AddMinutes(00);

        }
    }
}
