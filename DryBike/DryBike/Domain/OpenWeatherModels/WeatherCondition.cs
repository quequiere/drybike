﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Models.OpenWeatherModels
{
    public enum WeatherCondition : int
    {
        Unknown = 0,
        Clear = 1,
        Drizzle = 2,
        FewClouds = 3,
        ScatteredClouds = 4,
        BrokenClouds = 5,
        OvercastClouds = 6,
        Fog = 7,
        Rain = 8,
        ShowerRain = 9,
        FreezingRain = 10,
        Snow = 11,
        Thunderstorm = 12,
        Tornado = 13
    }
}
