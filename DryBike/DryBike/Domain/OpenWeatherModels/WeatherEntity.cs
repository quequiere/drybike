﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Models.OpenWeatherModels
{
    public class WeatherEntity
    {
        public WeatherEntity()
        {
            this.Forecasts = new List<WeatherForecastEntity>();
        }

        public DateTimeOffset ProcessDate { get; set; }

        public virtual List<WeatherForecastEntity> Forecasts { get; set; }
    }

}
