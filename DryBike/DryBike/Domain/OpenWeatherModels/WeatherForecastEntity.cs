﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Models.OpenWeatherModels
{
    public class WeatherForecastEntity
    {

        public WeatherCondition Condition { get; set; }

        public double CelsiusMinimum { get; set; }

        public double FahrenheitMinimum { get; set; }

        public double CelsiusMaximum { get; set; }

        public double FahrenheitMaximum { get; set; }

        public double? Humdity { get; set; }

        public double? Clouds { get; set; }

        public double? Rain { get; set; }

        public double? Snow { get; set; }

        public DateTimeOffset ForecastDate { get; set; }
    }
}
