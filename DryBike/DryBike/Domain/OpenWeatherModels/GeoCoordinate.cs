﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Models.OpenWeatherModels
{

    public class GeoCoordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public GeoCoordinate(double Latitude, double Longitude)
        {
            this.Latitude = Latitude;
            this.Longitude = Longitude;
        }
    }
}
