﻿using DryBike.Models.OpenWeatherModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DryBike.Web.weather
{
    public interface IWeatherService
    {
        Task<WeatherEntity> GetWeatherAsync(GeoCoordinate point);
    }

    public class OpenWeatherService : IWeatherService
    {
        private const string API_BASE_URL = "https://api.openweathermap.org/data/2.5/forecast?units=metric&lang=fr";

        private readonly string apiKey;

        private HttpClient httpClient;

        public OpenWeatherService(string apiKey)
        {
            this.apiKey = apiKey;
            this.httpClient = new HttpClient();
        }

        public async Task<WeatherEntity> GetWeatherAsync(GeoCoordinate point)
        {

            try
            {
                string response = await this.httpClient.GetStringAsync(this.BuildApiUrl(point));

                JObject json = JObject.Parse(response);

                if (json.SelectToken("cod").ToString() == "200")
                {
                    var weather = new WeatherEntity()
                    {
                        ProcessDate = DateTimeOffset.Now
                    };

                    foreach (var forecastJson in json["list"])
                    {
                        WeatherForecastEntity forecast = new WeatherForecastEntity();
                        forecast.Condition = GetWeatherCondition(forecastJson["weather"][0]["id"].Value<int>());
                        forecast.CelsiusMinimum = forecastJson["main"]["temp_min"].Value<double>();
                        forecast.FahrenheitMinimum = ConvertToFahrenheit(forecast.CelsiusMinimum);
                        forecast.CelsiusMaximum = forecastJson["main"]["temp_max"].Value<double>();
                        forecast.FahrenheitMaximum = ConvertToFahrenheit(forecast.CelsiusMaximum);
                        forecast.Humdity = forecastJson["main"]["humidity"].Value<double>();
                        forecast.Clouds = forecastJson["clouds"]?.SelectToken("all")?.Value<double>();
                        forecast.Rain = forecastJson["rain"]?.SelectToken("3h")?.Value<double>();
                        forecast.Snow = forecastJson["snow"]?.SelectToken("3h")?.Value<double>();
                        forecast.ForecastDate = DateTimeOffset.FromUnixTimeSeconds(forecastJson["dt"].Value<long>());

                        weather.Forecasts.Add(forecast);
                    }


                    return weather;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error in OpenWeatherApi " +e.Message);
                throw e;
            }



            return null;
        }

        private static WeatherCondition GetWeatherCondition(int code)
        {
            switch (code)
            {
                case int weatherCode
                    when weatherCode >= 200 && weatherCode < 300:
                    return WeatherCondition.Thunderstorm;
                case int weatherCode
                    when weatherCode >= 300 && weatherCode < 400:
                    return WeatherCondition.Drizzle;
                case int weatherCode
                    when weatherCode >= 500 && weatherCode < 510:
                    return WeatherCondition.Rain;
                case int weatherCode when weatherCode == 511:
                    return WeatherCondition.FreezingRain;
                case int weatherCode
                    when weatherCode >= 520 && weatherCode < 600:
                    return WeatherCondition.ShowerRain;
                case int weatherCode
                    when weatherCode >= 600 && weatherCode < 700:
                    return WeatherCondition.Snow;
                case int weatherCode when weatherCode == 781:
                    return WeatherCondition.Tornado;
                case int weatherCode
                    when weatherCode >= 700 && weatherCode < 800:
                    return WeatherCondition.Fog;
                case int weatherCode when weatherCode == 800:
                    return WeatherCondition.Clear;
                case int weatherCode when weatherCode == 801:
                    return WeatherCondition.FewClouds;
                case int weatherCode when weatherCode == 802:
                    return WeatherCondition.ScatteredClouds;
                case int weatherCode when weatherCode == 803:
                    return WeatherCondition.BrokenClouds;
                case int weatherCode when weatherCode == 804:
                    return WeatherCondition.OvercastClouds;
                default:
                    return WeatherCondition.Unknown;
            }
        }

        private string BuildApiUrl(GeoCoordinate point)
        {
            StringBuilder url = new StringBuilder();

            url.Append(API_BASE_URL);

            url.Append($"&lat={FormatDouble(point.Latitude)}&lon={FormatDouble(point.Longitude)}");

            url.Append($"&appid={this.apiKey}");

            return url.ToString();
        }

        private static string FormatDouble(double value)
        {
            return value.ToString().Replace(",", ".");
        }

        private double ConvertToFahrenheit(double celsius)
        {
            return Math.Round(((9.0 / 5.0) * celsius) + 32, 3);
        }

    }
}