﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Domain
{
    public enum MenuItemType
    {
        MyWeek,
        PathEditor,
        Radar,
        Config,
        About
        
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
