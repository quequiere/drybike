﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DryBike.Models
{
    public class UserPathModel
    {
        public string pathName { get; }
        public string dayDescription { get; }

        public int pathLocationNumber { get; }

        public string pathStart { get; }

        public bool isActive { get; set; }

        public UserPathModel(string pathName, string dayDescription, int pathLocationNumber, string pathStart,bool isActive)
        {
            this.pathName = pathName;
            this.dayDescription = dayDescription;
            this.pathLocationNumber = pathLocationNumber;
            this.pathStart = pathStart;
            this.isActive = isActive;

        }
    }
}
