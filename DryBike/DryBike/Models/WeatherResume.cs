﻿using System;
using System.Collections.Generic;
using System.Text;
using DryBike.Domain;
using Java.Util;

namespace DryBike.Models
{
    public class WeatherResume
    {
        public UserPath originalPath { get; set; }
        public DateTimeOffset startDate { get; set; }
        public string imageType { get; set; }

        public Guid id = Guid.NewGuid();

        public string dayOfTheWeek { get; set; }


    }
}
