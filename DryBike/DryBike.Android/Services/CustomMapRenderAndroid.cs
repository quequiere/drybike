﻿using System.Runtime.Remoting.Contexts;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using DryBike.Droid.Services;
using DryBike.Services;
using Java.IO;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomMapRender), typeof(CustomMapRenderAndroid))]
namespace DryBike.Droid.Services
{
    public class CustomMapRenderAndroid : MapRenderer, IOnMapReadyCallback
    {

        public CustomMapRenderAndroid(Android.Content.Context context) : base(context)
        {

        }

        // We use a native google map for Android
        private GoogleMap _map;

        public void OnMapReady(GoogleMap googleMap)
        {
            _map = googleMap;

            if (_map != null)
                _map.MapClick += googleMap_MapClick;

            googleMap.MyLocationEnabled = true;
            googleMap.UiSettings.CompassEnabled = true;
            googleMap.UiSettings.ZoomControlsEnabled = true;
            googleMap.UiSettings.MyLocationButtonEnabled = true;

            //Used to reaload gived path
            this.OnUpdatePolygoneAsked(this,new MapUpdatePolygoneEvent());
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            if (_map != null)
                _map.MapClick -= googleMap_MapClick;

            base.OnElementChanged(e);

            if (Control != null)
                ((MapView) Control).GetMapAsync(this);

            CustomMapRender custom = (CustomMapRender)e.NewElement;
            custom.UpdatePolygone += OnUpdatePolygoneAsked;
        }

        private void googleMap_MapClick(object sender, GoogleMap.MapClickEventArgs e)
        {
            ((CustomMapRender) Element).OnTap(new Position(e.Point.Latitude, e.Point.Longitude));
        }

        private void OnUpdatePolygoneAsked(object sender, MapUpdatePolygoneEvent e)
        {
            System.Console.WriteLine("Redraw polygon");

            CustomMapRender customMapRender = (CustomMapRender)Element;


            var polylineOptions = new PolylineOptions();
            polylineOptions.InvokeColor(0x66FF0000);

            foreach (var position in customMapRender.RouteCoordinates)
            {
                polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));
            }

            _map.AddPolyline(polylineOptions);

        }
    }
}