﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace DryBike.Droid
{
    [Activity(Label = "DryBike", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            string[] perms = new string[]
            {
                Android.Manifest.Permission.AccessFineLocation,
                Android.Manifest.Permission.Internet,
                Android.Manifest.Permission.WriteExternalStorage,
                Android.Manifest.Permission.AccessCoarseLocation,
                Android.Manifest.Permission.AccessFineLocation,
                Android.Manifest.Permission.AccessLocationExtraCommands,
                Android.Manifest.Permission.AccessMockLocation,
                Android.Manifest.Permission.AccessNetworkState,
                Android.Manifest.Permission.AccessWifiState,

            };


            this.RequestPermissions(perms, 1);

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }
    }
}