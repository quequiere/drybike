﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Controls.Maps;
using DryBike.Services;
using DryBike.UWP.Services;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.UWP;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(CustomMapRender), typeof(CustomMapRenderUwp))]
namespace DryBike.UWP.Services
{
    public class CustomMapRenderUwp : MapRenderer
    {
        MapControl nativeMap;

        public CustomMapRenderUwp()
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement==null)
                return;

            Debug.WriteLine("Element changed on map");

            Control.MapTapped += bingMapTapped;

            CustomMapRender custom = (CustomMapRender) e.NewElement;
            custom.UpdatePolygone += OnUpdatePolygoneAsked;

            //Added to refresh at the first start
            this.OnUpdatePolygoneAsked(this,new MapUpdatePolygoneEvent());
        }

        private void OnUpdatePolygoneAsked(object sender, MapUpdatePolygoneEvent e)
        {
            Debug.WriteLine("Re-draw polygone");

            var formsMap = (CustomMapRender)Element;
            var nativeMap = Control as MapControl;

            nativeMap.MapElements.Clear();

            if (formsMap.RouteCoordinates.Count <= 0)
                return;

            var coordinates = new List<BasicGeoposition>();
            foreach (var position in formsMap.RouteCoordinates)
            {
                coordinates.Add(new BasicGeoposition() { Latitude = position.Latitude, Longitude = position.Longitude });
            }

            var polyline = new MapPolyline();
            polyline.StrokeColor = Windows.UI.Color.FromArgb(128, 255, 0, 0);
            polyline.StrokeThickness = 5;
            polyline.Path = new Geopath(coordinates);
            nativeMap.MapElements.Add(polyline);
        }

        protected void bingMapTapped(MapControl mapControl, MapInputEventArgs e)
        {
            ((CustomMapRender)Element).OnTap(new Position(e.Location.Position.Latitude, e.Location.Position.Longitude));

        }

   
    }
}
